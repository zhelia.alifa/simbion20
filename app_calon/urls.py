from django.conf.urls import url
from .views import index, daftar, insertDaftar, daftar
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^pendafaran/$', daftar, name='pendafaran'),
    url(r'^insert', insertDaftar, name='insertDaftar'),
    url(r'^daftar', daftar, name='daftar')
]
