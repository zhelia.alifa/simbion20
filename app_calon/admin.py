from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Admin)
admin.site.register(Donatur)
admin.site.register(IndividualDonor)
admin.site.register(Mahasiswa)
admin.site.register(Pembayaran)
admin.site.register(Pendaftaran)
admin.site.register(Pengguna)
admin.site.register(Pengumuman)
admin.site.register(RiwayatAkademik)
admin.site.register(SkemaBeasiswa)
admin.site.register(SkemaBeasiswaAktif)
admin.site.register(SyaratBeasiswa)
admin.site.register(TempatWawancara)
admin.site.register(Wawancara)
admin.site.register(Yayasan)
