from django import forms
from .models import *

class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        text = str(obj.kode_skema_beasiswa.nama) + " - " + str(obj.no_urut)
        return text

class DaftarForm(forms.Form):
    error_messages = {
        'required': 'Please input',
    }

    npm = {
        'type':'text',
        'class': 'form-control',
        'placeholder': 'Enter NPM'
    }
    email = {
        'type':'text',
        'class': 'form-control',
        'placeholder': 'Enter Email'
    }
    ips = {
        'type':'text',
        'class': 'form-control',
        'placeholder': 'Enter IPS'
    }

    kode_skema_beasiswa = MyModelChoiceField(queryset=SkemaBeasiswaAktif.objects.filter(status="buka").order_by( 'no_urut', 'kode_skema_beasiswa'), required=True,
		empty_label="Select Kode Skema Beasiswa", label='Kode Skema', widget=forms.Select(attrs={'class':'form-control'}))
    npm = forms.CharField(label='NPM', required=True, widget=forms.TextInput(attrs=npm))
    email = forms.CharField(label='Email', required=True, widget=forms.TextInput(attrs=email))
    ips = forms.FloatField(label='IPS', required=True, widget=forms.TextInput(attrs=ips))
