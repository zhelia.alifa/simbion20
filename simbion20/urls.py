"""simbion20 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin
from django.conf.urls import include
import app_calon.urls as app_calon
import app_skema_beasiswa.urls as app_skema_beasiswa
import app_info_beasiswa.urls as app_info_beasiswa
import app_seleksi.urls as app_seleksi
import app_login.urls as app_login
import app_detail.urls as app_detail
import app_register.urls as app_register
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^app-calon/', include(app_calon,namespace='app-calon')),
    url(r'^app-seleksi/', include(app_seleksi,namespace='app-seleksi')),
    url(r'^app-skema-beasiswa/', include(app_skema_beasiswa,namespace='app-skema-beasiswa')),
    url(r'^app-info-beasiswa/', include(app_info_beasiswa,namespace='app-info-beasiswa')),
    url(r'^app-login/', include(app_login,namespace='app-login')),
    url(r'^app-detail/', include(app_detail,namespace='app-detail')),
    url(r'^app-register/', include(app_register,namespace='app-register')),
    url(r'^$', RedirectView.as_view(url='/app-info-beasiswa/', permanent=True), name='redirect_landing_page'),
]
