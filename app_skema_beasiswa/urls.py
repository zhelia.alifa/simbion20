from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_form_paket/$', add_form_paket, name='add_form_paket'),
    url(r'^add_paket/$', add_paket, name='add_paket'),
    url(r'^add_skemabeasiswa', add_skemabeasiswa, name='add_skemabeasiswa'),
    url(r'^add_paketbeasiswa', add_paketbeasiswa, name='add_paketbeasiswa'),
]