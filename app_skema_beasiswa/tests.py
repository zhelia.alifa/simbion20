from django.test import TestCase
from django.test import Client
from django.urls import resolve

# Create your tests here.
class AppSkemaBeasiswaUnitTest(TestCase):
	def test_app_skema_beasiswa_url_is_exist(self):
		response = Client().get('/app-skema-beasiswa/')
		self.assertEqual(response.status_code, 200)