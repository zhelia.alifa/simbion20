from django import forms
from app_info_beasiswa.models import *

class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        text = str(obj.kode)
        return text

class DaftarPaket(forms.Form):
    error_messages = {
        'required': 'Please input',
    }

    no_urut = {
        'type':'text',
        'class': 'form-control',
        'placeholder': 'Enter no urut'
    }

    tgl_mulai_daftar = {
        'type':'date',
        'class': 'form-control',
        'placeholder': 'yyyy-mm-dd'
    }

    tgl_tutup_daftar = {
        'type':'date',
        'class': 'form-control',
        'placeholder': 'yyyy-mm-dd'
    }   

    kode_skema_beasiswa = MyModelChoiceField(queryset=SkemaBeasiswa.objects.order_by('kode')
        ,empty_label="Select Kode Skema Beasiswa", label='Kode Skema', required=True, widget=forms.Select(attrs={'class':'form-control'}))
    no_urut = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=no_urut))
    tgl_mulai_daftar = forms.DateTimeField(label='', required=True, widget=forms.DateInput(tgl_mulai_daftar))
    tgl_tutup_daftar = forms.DateTimeField(label='', required=True, widget=forms.DateInput(tgl_tutup_daftar))

class Todo_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    kode_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan kode...'
    }
    namabeasiswa_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan nama beasiswa...'
    }
    jenisbeasiswa_choices = [
        ('beasiswa prestasi akademik', 'Beasiswa Prestasi Akademik'),
        ('beasiswa bantuan belajar', 'Beasiswa Bantuan Belajar'),
        ('beasiswa tugas akhir', 'Beasiswa Tugas Akhir'),
        ]
    deskripsi_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan deskripsi...'
    }
    syaratbeasiswa_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan syarat beasiswa...'
    }

    kode = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=kode_attrs))
    namabeasiswa = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=namabeasiswa_attrs))
    jenisbeasiswa = forms.CharField(label='', required=True, widget=forms.Select(choices = jenisbeasiswa_choices))
    deskripsi = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=deskripsi_attrs))
    syaratbeasiswa = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=syaratbeasiswa_attrs))