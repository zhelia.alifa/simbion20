from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from app_info_beasiswa.models import SkemaBeasiswa, SkemaBeasiswaAktif, SyaratBeasiswa
from .forms import *
import datetime


# Create your views here.
response = {}
def index(request):
	response['skema_beasiswa'] = SkemaBeasiswa.objects.raw('SELECT * FROM skema_beasiswa')
	html = 'app_skema_beasiswa.html'
	return render(request, html, response)

def add_form_paket(request):
	response['todo_form'] = Todo_Form
	html = 'app_form_paket.html'
	return render(request, html, response)

def add_paket(request):
	html = 'app_tambah_paket.html'
	response['daftar_paket'] = DaftarPaket
	return render(request, html, response)

@csrf_exempt
def add_skemabeasiswa(request):
	form = Todo_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		cursor = connection.cursor()
		kode = request.POST['kode']
		namabeasiswa = request.POST['namabeasiswa']
		jenisbeasiswa = request.POST['jenisbeasiswa']
		deskripsi = request.POST['deskripsi']
		syaratbeasiswa = request.POST['syaratbeasiswa']
		no_identitas = 2;

		query_insert = "INSERT INTO skema_beasiswa (kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES (%d, '%s', '%s', '%s', %d)" %\
		(int(kode), namabeasiswa, jenisbeasiswa, deskripsi, int(no_identitas))
		cursor.execute(query_insert)
		query_insert_lagi = "INSERT INTO syarat_beasiswa (kode_beasiswa,syarat) VALUES (%d, '%s')" %\
		(int(kode), syaratbeasiswa)
		cursor.execute(query_insert_lagi)
		cursor.close()

		'''todo = SkemaBeasiswa(kode=kode,nama=namabeasiswa,jenis=jenisbeasiswa,deskripsi=deskripsi,nomor_identitas_donatur='1')
		todo.save()
		messages.success(request, "New Skema Beasiswa!")'''
		return HttpResponseRedirect('/app-skema-beasiswa/')
	else:
		return HttpResponseRedirect('/app-skema-beasiswa/')

@csrf_exempt
def add_paketbeasiswa(request):
	form = DaftarPaket(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		cursor = connection.cursor()
		kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
		no_urut = request.POST['no_urut']
		tgl_mulai_daftar = request.POST['tgl_mulai_daftar']
		tgl_tutup_daftar = request.POST['tgl_tutup_daftar']

		now = datetime.datetime.now()
		waktu_skrg = now.strftime("%Y-%m-%d")
		print(waktu_skrg)

		status = "ditutup"

		if tgl_tutup_daftar > waktu_skrg:
			status="dibuka"
		query_insert = "INSERT INTO skema_beasiswa_aktif (kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status,jumlah_pendaftar) VALUES (%d, %d, '%s', '%s', 'April - Juni 2018', '%s', 0)" %\
		(int(kode_skema_beasiswa), int(no_urut), tgl_mulai_daftar, tgl_tutup_daftar, status)
		cursor.execute(query_insert)
		cursor.close()

		return HttpResponseRedirect('/app-skema-beasiswa/')
	else:
		return HttpResponseRedirect('/app-skema-beasiswa/')