from django.apps import AppConfig


class AppSkemaBeasiswaConfig(AppConfig):
    name = 'app_skema_beasiswa'
