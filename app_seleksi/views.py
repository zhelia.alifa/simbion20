from django.shortcuts import render
from django.http import HttpResponseRedirect
from app_calon.models import *
from django.db import connection

response = {}
id = 2
# Create your views here.
def index(request):
	cursor = connection.cursor()
	username = request.session['username']
	print(username)

	# skema = SkemaBeasiswa.objects.raw('SELECT * FROM skema_beasiswa WHERE nomor_identitas_donatur = %d', [id])
	# query = 'SELECT * FROM skema_beasiswa_aktif'
	# cursor.execute (query)
	# skema = cursor.fetchall()
	# print(skema)
	# query = "SELECT * from donatur where username='{}'".format(username)
	nomor_identitas = Donatur.objects.raw("SELECT * from donatur where username='" +username+ "'")[0]
	id = nomor_identitas.nomor_identitas
	skema_beasiswa_buka = SkemaBeasiswaAktif.objects.raw("SELECT * from skema_beasiswa, skema_beasiswa_aktif where skema_beasiswa_aktif.kode_skema_beasiswa = skema_beasiswa.kode and skema_beasiswa.nomor_identitas_donatur='"+id+"'")

	response['skema_beasiswa'] = skema_beasiswa_buka
	# skema = SkemaBeasiswaAktif.objects.raw("SELECT * from skema_beasiswa_aktif sba, skema_beasiswa sb, donatur d, pengguna p where sb.kode = sba.kode_skema_beasiswa and sb.nomor_identitas_donatur = '{}'".format(nomor_identitas))
	# skema_beasiswa_aktif = SkemaBeasiswaAktif.objects.raw('SELECT * FROM skema_beasiswa_aktif')
	#
	# # query = 'SELECT * from skema_beasiswa_aktif WHERE kode_skema_beasiswa='+str(skema_beasiswa_aktif[2].kode_skema_beasiswa.kode)
	# # skema_beasiswa = cursor.execute(query)
	# response['skema_beasiswa'] = skema
	html = 'app_seleksi/list_beasiswa.html'
	return render(request, html, response)

def seleksi(request):
	cursor = connection.cursor()
	query = request.GET.get('kode')
	kode = "{}".format(query)

	cursor.execute("SELECT no_urut FROM skema_beasiswa_aktif WHERE status = 'buka' AND kode_skema_beasiswa=" + kode)
	no_urut = cursor.fetchone()[0]

	pendaftaran = Pendaftaran.objects.raw("SELECT * FROM pendaftaran WHERE kode_skema_beasiswa="+kode+" and no_urut=" + str(no_urut))
	response['pendaftaran'] = pendaftaran
	html = 'app_seleksi/seleksi.html'
	return render(request, html, response)

def terima(request):
	cursor = connection.cursor()
	get = request.GET.get('kode')
	kode = "{}".format(get)
	cursor.execute("SELECT no_urut FROM skema_beasiswa_aktif WHERE status = 'buka' AND kode_skema_beasiswa=" + kode)
	no_urut = cursor.fetchone()[0]
	query_update = "UPDATE pendaftaran SET status_terima= 'aktif' where kode_skema_beasiswa= "+kode+" and no_urut=" + str(no_urut)
	cursor.execute(query_update)
	html = 'app_seleksi/list_beasiswa.html'
	return render(request, html, response)

def tolak(request):
	cursor = connection.cursor()
	get = request.GET.get('kode')
	kode = "{}".format(get)
	cursor.execute("SELECT no_urut FROM skema_beasiswa_aktif WHERE status = 'buka' AND kode_skema_beasiswa=" + kode)
	no_urut = cursor.fetchone()[0]
	query_update = "UPDATE pendaftaran SET status_terima= 'tidak aktif' where kode_skema_beasiswa= "+kode+" and no_urut=" + str(no_urut)
	cursor.execute(query_update)
	html = 'app_seleksi/list_beasiswa.html'
	return render(request, html, response)
