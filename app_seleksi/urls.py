from django.conf.urls import url
from .views import index, seleksi, terima, tolak
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^seleksi', seleksi, name='seleksi'),
    url(r'^terima', terima, name='terima'),
    url(r'^tolak', tolak, name='tolak'),
]
