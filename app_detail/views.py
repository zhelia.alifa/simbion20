from django.shortcuts import render
from app_info_beasiswa.models import SyaratBeasiswa, SkemaBeasiswa
# Create your views here.

response = {}
def index(request): 
    query = request.GET.get('kode')
    kode = "{}".format(query)
    
    response['SkemaBeasiswa'] = SkemaBeasiswa.objects.raw("SELECT * FROM skema_beasiswa WHERE kode="+kode)
    response['SyaratBeasiswa'] = SyaratBeasiswa.objects.raw("SELECT * FROM syarat_beasiswa WHERE kode_beasiswa="+kode)

    html = 'app_detail/app_detail.html'
    return render(request, html, response)