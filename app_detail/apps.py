from django.apps import AppConfig


class AppDetailConfig(AppConfig):
    name = 'app_detail'
