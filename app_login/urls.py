from django.conf.urls import url
from .views import index, auth_login, auth_logout
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^auth_login/$', auth_login, name="auth_login"),
    url(r'^auth_logout/$', auth_logout, name="auth_logout"),
]
