from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from django.db import connection, connections

response = {}
# Create your views here.
def index(request):
	html = 'app_login/app_login.html'
	return render(request, html, response)

# def index(request):
# 	if("username" not in request.session):
# 		return render(request, 'app_login/app_login.html', response)
# 	else:
# 		return HttpResponseRedirect(reverse('app-info-beasiswa:index'))

def hasNumbers(inputString):
	return any(char.isdigit() for char in inputString)

def auth_login(request):

    if request.method == "POST":

        username = request.POST['username']
        password = request.POST['password']

        cursor = connection.cursor()
        query = "select username, role from pengguna where username = '" + username + "' and password = '" + password + "'"
        cursor.execute(query)

        result = cursor.fetchone()
        print(result)

        if(result is not None):
            request.session["username"] = result[0]
            request.session["is_login"] = "login"

            request.session["role"] = result[0]

            if(result[1] == 'donatur' ):
            	return HttpResponseRedirect(reverse('app-skema-beasiswa:index'))
            elif(result[1] == 'mahasiswa'):
            	return HttpResponseRedirect(reverse('app-calon:index'))

        username = request.POST['username']
        password = request.POST['password']
        cursor = connection.cursor()
        query = "select username, role from pengguna where username = '" + username + "' and password = '" + password + "'"
        cursor.execute(query)
        result = cursor.fetchone()
        print(result)

        if(result is not None):
            request.session["username"] = result[0]
            request.session["role"] = result[0]

            if(result[1] == 'donatur' ):
                messages.error(request, "Anda terlogin sebagai Donatur")
                return HttpResponseRedirect(reverse('app-skema-beasiswa:index'))
            elif(result[1] == 'mahasiswa'):
                messages.error(request, "Anda terlogin sebagai Mahasiswa")
                return HttpResponseRedirect(reverse('app-calon:index'))
		# elif(result is not None and password is None):
		# 	messages.error(request, "Username atau password salah")
		# 	return HttpResponseRedirect(reverse('app-login:auth_login'))
		# else:
		# 	messages.error(request, "Anda belum mendaftar")
		# 	return HttpResponseRedirect(reverse('app-login:auth_login'))
		# return HttpResponseRedirect(reverse('app-login:index'))

def auth_logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('app-info-beasiswa:index'))
