from django.shortcuts import render
#from django.db import connection
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import SkemaBeasiswaAktif, SkemaBeasiswa
import datetime
from django.db import connection

# Create your views here.
response = {}
def index(request):

	cursor = connection.cursor()

	skema_beasiswa_aktif = SkemaBeasiswaAktif.objects.raw('SELECT * FROM skema_beasiswa_aktif')

	now = datetime.date.today()
	waktu_skrg = now.strftime("%Y-%m-%d")


	for i in skema_beasiswa_aktif:
		if i.tgl_tutup_pendaftaran < now:
			query_update = "UPDATE skema_beasiswa_aktif SET status= 'ditutup' where kode_skema_beasiswa= "+str(i.kode_skema_beasiswa.kode) +" and no_urut="+ str(i.no_urut)
			cursor.execute(query_update)

	response['skema_beasiswa_aktif'] = skema_beasiswa_aktif
	html = 'app_info_beasiswa/app_info_beasiswa.html'
	return render(request, html, response)