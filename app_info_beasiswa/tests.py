from django.test import TestCase
from django.test import Client
from django.urls import resolve

# Create your tests here.
class AppInfoBeasiswaUnitTest(TestCase):
	def test_app_info_beasiswa_url_is_exist(self):
		response = Client().get('/app-info-beasiswa/')
		self.assertEqual(response.status_code, 200)