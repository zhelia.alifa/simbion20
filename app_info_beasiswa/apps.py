from django.apps import AppConfig


class AppInfoBeasiswaConfig(AppConfig):
    name = 'app_info_beasiswa'
