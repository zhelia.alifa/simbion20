from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection, connections


# Create your views here.
response = {}
def index(request):
	response['author'] = "BASDAT 20"
	html = 'daftar-pilihan.html'
	return render(request, html, response)

def add_form_mahasiswa(request):
	html = 'form-mahasiswa-register.html'
	return render(request, html, response)

def add_form_individual(request):
	html = 'individualDonatur-register.html'
	return render(request, html, response)

def add_form_yayasan(request):
	html = 'YayasanDonatur-register.html'
	return render(request, html, response)
	

def cek_form_individual(request):
	if request.method == 'POST':

		username = request.POST['uname_mhs']
		password = request.POST['pass_mhs']
		no_id = request.POST["npm"]
		email = request.POST["email"]
		nama_lengkap = request.POST["nama_lengkap"]
		npwp = request.POST["alamat_tinggal"]
		no_tel = request.POST["alamat_domisili"]
		alamat = request.POST["instansi_bank"]


		cursor = connection.cursor()
		query = "select username from DONATUR where username = '" + username + "'"
		cursor.execute(query)
		usern_do = cursor.fetchone()

		cursor = connection.cursor()
		query = "select npm from MAHASISWA where npm = '" + npm + "'"
		cursor.execute(query)
		npm_mahasiswa = cursor.fetchone()

		if (usern_do is not None):
			messages.error(request, "Username sudah teregister")
		elif (no_id is None):
			messages.error(request, "Nomor identitas sudah teregister")

		cursor = connection.cursor()
		query = "select password from pengguna where password = '" + password + "'"
		cursor.execute(query)
		pass_mahasiswa = cursor.fetchone()

		query_pengguna = "INSERT INTO PENGGUNA (username, password, role) VALUES "+ "('" + username + "', '" + request.POST["pass_mhs"] + "', 'mahasiswa');"
		cursor = connection.cursor()
		cursor.execute(query_pengguna)
		return HttpResponseRedirect(reverse('app-login:index'))

		query = "INSERT INTO DONATUR(nomor_identitas, email, nama, npwp, no_telp, alamat, username) VALUES "+"(" + request.POST["no_id"] + ", '" + request.POST["email"] + "', '" + request.POST["nama_lengkap"] + "', " + request.POST["npwp"] + ", '" + request.POST["no_tel"] + "', '" + request.POST["alamat"] + "', '" + request.POST["uname_mhs"] + "');"
		cursor = connection.cursor()
		cursor.execute(query)

	return HttpResponseRedirect(reverse('app-login:index'))

def cek_form_mahasiswa(request):
	if request.method == 'POST':

		username = request.POST['uname_mhs']
		password = request.POST['pass_mhs']
		npm = request.POST["npm"]
		email = request.POST["email"]
		nama_lengkap = request.POST["nama_lengkap"]
		nomor_telepon = request.POST["nomor_telepon"]
		alamat_tinggal = request.POST["alamat_tinggal"]
		alamat_domisili = request.POST["alamat_domisili"]
		instansi_bank = request.POST["instansi_bank"]
		nomor_rekening = request.POST["nomor_rekening"]
		nama_pemilik = request.POST["nama_pemilik"]


		cursor = connection.cursor()
		query = "select username from MAHASISWA where username = '" + username + "'"
		cursor.execute(query)
		usern_mahasiswa = cursor.fetchone()

		cursor = connection.cursor()
		query = "select npm from MAHASISWA where npm = '" + npm + "'"
		cursor.execute(query)
		npm_mahasiswa = cursor.fetchone()

		if (username is not None):
			messages.error(request, "Username sudah teregister")
		elif (npm_mahasiswa is None):
			messages.error(request, "NIK sudah teregister")

		cursor = connection.cursor()
		query = "select password from pengguna where password = '" + password + "'"
		cursor.execute(query)
		pass_mahasiswa = cursor.fetchone()

		query_pengguna = "INSERT INTO PENGGUNA (username, password, role) VALUES "+ "('" + username + "', '" + request.POST["pass_mhs"] + "', 'mahasiswa');"
		cursor = connection.cursor()
		cursor.execute(query_pengguna)
		return HttpResponseRedirect(reverse('app-login:index'))

		query = "INSERT INTO MAHASISWA(npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username) VALUES "+"(" + request.POST["npm"] + ", '" + request.POST["email"] + "', '" + request.POST["nama_lengkap"] + "', " + request.POST["nomor_telepon"] + ", '" + request.POST["alamat_tinggal"] + "', '" + request.POST["alamat_domisili"] + "', '" + request.POST["instansi_bank"] + "', " + request.POST["nomor_rekening"] + ", '" + request.POST["nama_pemilik"] + "', '" + request.POST["uname_mhs"] + "');"
		cursor = connection.cursor()
		cursor.execute(query)

	return HttpResponseRedirect(reverse('app-login:index'))


def cek_form_yayasan(request):
	if request.method == 'POST':

		username = request.POST['uname_mhs']
		password = request.POST['pass_mhs']
		no_id = request.POST["npm"]
		email = request.POST["email"]
		nama_lengkap = request.POST["nama_lengkap"]
		npwp = request.POST["alamat_tinggal"]
		no_tel = request.POST["alamat_domisili"]
		alamat = request.POST["instansi_bank"]


		cursor = connection.cursor()
		query = "select username from DONATUR where username = '" + username + "'"
		cursor.execute(query)
		usern_do = cursor.fetchone()

		cursor = connection.cursor()
		query = "select no_identitas from DONATUR where no_identitas = '" + no_id + "'"
		cursor.execute(query)
		npm_mahasiswa = cursor.fetchone()

		if (usern_do is not None):
			messages.error(request, "Username sudah teregister")
		elif (no_id is None):
			messages.error(request, "Nomor identitas sudah teregister")

		cursor = connection.cursor()
		query = "select password from pengguna where password = '" + password + "'"
		cursor.execute(query)
		pass_mahasiswa = cursor.fetchone()

		query_pengguna = "INSERT INTO PENGGUNA (username, password, role) VALUES "+ "('" + username + "', '" + request.POST["pass_mhs"] + "', 'mahasiswa');"
		cursor = connection.cursor()
		cursor.execute(query_pengguna)
		return HttpResponseRedirect(reverse('app-login:index'))

		query = "INSERT INTO DONATUR(nomor_identitas, email, nama, npwp, no_telp, alamat, username) VALUES "+"(" + request.POST["no_id"] + ", '" + request.POST["email"] + "', '" + request.POST["nama_lengkap"] + "', " + request.POST["npwp"] + ", '" + request.POST["no_tel"] + "', '" + request.POST["alamat"] + "', '" + request.POST["uname_mhs"] + "');"
		cursor = connection.cursor()
		cursor.execute(query)

	return HttpResponseRedirect(reverse('app-login:index'))