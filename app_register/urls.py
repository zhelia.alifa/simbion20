from django.conf.urls import url
from .views import index, add_form_mahasiswa, add_form_individual, add_form_yayasan, cek_form_mahasiswa, cek_form_yayasan, cek_form_individual

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_form_mahasiswa/$', add_form_mahasiswa, name='add_form_mahasiswa'),
    url(r'^add_form_individual/$', add_form_individual, name='add_form_individual'),
    url(r'^add_form_yayasan/$', add_form_yayasan, name='add_form_yayasan'),
    url(r'^cek_form_mahasiswa/$', cek_form_mahasiswa, name='cek_form_mahasiswa'),
    url(r'^cek_form_yayasan/$', cek_form_yayasan, name='cek_form_yayasan'),
    url(r'^cek_form_individual/$', cek_form_individual, name='cek_form_individual'),
]